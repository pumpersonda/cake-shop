var gulp = require('gulp');
var ts = require('gulp-typescript');
var tslint = require('gulp-tslint');
var minify = require('gulp-minify');
var tsProject = ts.createProject('tsconfig.json', {outFile: 'output.js', module: "system"});
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var clean = require('gulp-clean');


gulp.task('clean', function () {
  return gulp.src('dist')
    .pipe(clean({force: true}))
    .pipe(gulp.dest('dist'));
});


gulp.task('compile', ['clean'], function () {
  return gulp.src('src/**/*.ts')
    .pipe(tslint({
      formatter: "verbose"
    }))
    .pipe(tslint.report())
    .pipe(tsProject())
    .pipe(gulp.dest('dist'));
});


gulp.task('compress', ['compile','compress-css'], function () {
  return gulp.src('dist/output.js')
    .pipe(minify({
      ext: {
        src: '-debug.js',
        min: '.js'
      },
      exclude: ['tasks'],
      ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('compress-css', function () {
  return gulp.src('src/**/*.css')
    .pipe(cleanCSS({compatibility: '*', merging: true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('rename', ['compress'], function () {
  return gulp.src('dist/output.js')
    .pipe(rename('dist/index.js'))
    .pipe(gulp.dest('.'));
});

gulp.task('default', ['rename']);

import {ProductPage} from './pages/product.po';
import {browser, by} from 'protractor';


describe('cake-shop Product', () => {
  let page: ProductPage;

  beforeEach(() => {
    page = new ProductPage();
  });

  it('should display title Products', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Products');
  });

  it('should open "New Modal Product"', () => {
    page.navigateTo();
    page.geNewProductButton().click();
    expect(page.getModalProduct().isPresent()).toBeTruthy();

  });

  it('should has the "/#/product/new" URL', () => {
    page.navigateTo();
    page.geNewProductButton().click();
    expect(browser.getCurrentUrl()).toMatch('/#/product/new');
  });


  it('should close the modal', () => {
    page.navigateTo();
    page.geNewProductButton().click();
    page.getCloseButton().click();
    expect(browser.getCurrentUrl()).toMatch('/#/product');
  });




});

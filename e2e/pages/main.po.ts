import {browser, by, element} from 'protractor';

export class MainPage {
  navigateTo() {
    return browser.get('/#/login');
  }

  navigateToHome() {
    return browser.get('/#/');
  }

  // NavBar
  getAccountSection() {
    return element(by.css('#dropdownBasic1'));
  }

  getClientButtonFromNavBar() {
    return element(by.css('a[routerLink="client"]'));
  }

  getSignUpButtonFromNavBar() {
    return element(by.css('#sign-up'));
  }

  getLogoutButtonFromNavBar() {
    return element(by.css('#logout'));
  }

  // Modal login
  getEmailInput() {
    return element(by.css('#field_email'));
  }

  getPasswordInput() {
    return element(by.css('#field_password'));
  }

  getSaveButton() {
    return element(by.css('button[type="submit"]'));
  }

}

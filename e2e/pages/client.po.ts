import {browser, by, element} from 'protractor';
import {MainPage} from './main.po';

export class ClientPage extends MainPage {
  navigateTo() {
    return browser.get('/#/client');
  }
}

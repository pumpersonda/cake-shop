import {browser, by, element} from 'protractor';

export class ProductPage {
  navigateTo() {
    return browser.get('/#/product');
  }

  getTitleText() {
    return element(by.css('h2')).getText();
  }

  geNewProductButton() {
    return element(by.buttonText('New'));
  }

  getModalProduct() {
    return element(by.css('#product-form'));
  }

  getInputName() {
    return element(by.css('#product-form'));
  }

  getInputPrice() {
    return element(by.css('#field_price'));
  }

  getInputDescription() {
    return element(by.css('#field_des'));
  }

  getInputIsAvailable() {
    return element(by.css('#field_available'));
  }

  getSaveButton() {
    return element.all(by.css('#saveProduct')).first();
  }

  getCloseButton() {
    return element(by.css('#close'));
  }

  getTable() {
    return element.all(by.css('#productsList th')).map(elm => elm.getText());
  }
}

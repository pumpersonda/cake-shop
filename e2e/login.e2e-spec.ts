import {browser} from 'protractor';
import {MainPage} from './pages/main.po';


describe('cake-shop Login', () => {
  let page: MainPage;
  beforeEach(() => {
    page = new MainPage();
  });

  it('should open the modal login by button', () => {
    page.navigateToHome();
    page.getAccountSection().click();
    page.getSignUpButtonFromNavBar().click();
    expect(browser.getCurrentUrl()).toMatch('/#/login');
  });

  it('should hide the logout option', () => {
    page.navigateToHome();
    expect(page.getLogoutButtonFromNavBar().isDisplayed()).toBeFalsy();
  });
  it('should redirect to home after login', () => {
    page.navigateToHome();
    page.getAccountSection().click();
    page.getSignUpButtonFromNavBar().click();
    page.getEmailInput().clear().sendKeys('q@hotmail.com');
    page.getPasswordInput().clear().sendKeys('my-password');
    page.getSaveButton().click();
    expect(browser.getCurrentUrl()).toMatch('/#/');
  });
});

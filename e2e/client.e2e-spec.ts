import {ClientPage} from './pages/client.po';
import {browser} from 'protractor';


describe('cake-shop Client before login', () => {
  let page: ClientPage;

  beforeEach(() => {
    page = new ClientPage();
  });

  it('should redirect to home', () => {
    page.navigateTo();
    expect(browser.getCurrentUrl()).toMatch('/#/');
  });
});


describe('cake-shop Client after login', () => {
  let clientPage: ClientPage;

  beforeEach(() => {
    clientPage = new ClientPage();
    clientPage.navigateToHome();
    clientPage.getAccountSection().click();
    clientPage.getSignUpButtonFromNavBar().click();
    clientPage.getEmailInput().sendKeys('q@hotmail.com');
    clientPage.getPasswordInput().sendKeys('my-password');
    clientPage.getSaveButton().click();
    clientPage.getClientButtonFromNavBar().click();
  });

  it('should authorize to visit /client', () => {
    expect(browser.getCurrentUrl()).toMatch('/#/client');
  });
});

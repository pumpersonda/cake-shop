import {TestBed, async} from '@angular/core/testing';
import {ProductService} from '../services/product.service';
import {RouterTestingModule} from '@angular/router/testing';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {Product} from '../models/product.model';
import {ProductDeleteDialogComponent, ProductDeletePopupComponent} from './product-delete-dialog.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';


describe('ProductDeleteDialogComponent', function () {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductDeleteDialogComponent,
        ProductDeletePopupComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        ProductService,
        NgbActiveModal]
    }).compileComponents();
  }));

  it('should create the ProductDeleteDialogComponent', async(() => {
    const fixture = TestBed.createComponent(ProductDeleteDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should create a form', async(() => {
    const fixture = TestBed.createComponent(ProductDeleteDialogComponent);
    const app = fixture.debugElement.componentInstance;
    app.product = new Product();
    fixture.detectChanges();
    const element = fixture.debugElement.nativeElement;
    expect(element.querySelector('form')).toBeTruthy();
  }));

});

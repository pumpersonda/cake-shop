import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../services/product.service';
import {Product} from '../models/product.model';

@Component({
  selector: 'app-product-delete-dialog',
  templateUrl: './product-delete-dialog.component.html'
})
export class ProductDeleteDialogComponent {
  product: Product;

  constructor(private productService: ProductService,
              public activeModal: NgbActiveModal) {

  }

  confirmDelete() {
    this.productService.delete(this.product.id);
    this.activeModal.dismiss('Product deleted!');
  }

  clear() {
    this.activeModal.dismiss('Cancel');
  }

}


@Component({
  selector: 'app-product-delete-popup',
  template: ''
})
export class ProductDeletePopupComponent implements OnInit, OnDestroy {
  routeSub: any;

  constructor(private route: ActivatedRoute,
              private modalService: NgbModal,
              private router: Router,
              private productService: ProductService) {
  }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe((params) => {
      if (params['id']) {
        this.productService.find(params['id'])
          .subscribe(
            (response) => {
              const product: Product = response.body as Product;
              product.id = params['id'];
              this.openModal(Object.assign({}, product));
            },
            (err) => console.log(err)
          );
      }
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  private openModal(product: Product) {
    setTimeout(() => {
      const modalRef = this.modalService.open(ProductDeleteDialogComponent as Component, {size: 'lg', backdrop: 'static'});
      modalRef.componentInstance.product = product;
      modalRef.result.then((result) => {
        this.router.navigate(['/product']);
      }, (reason) => {
        this.router.navigate(['/product']);
      });
    }, 0);
  }
}

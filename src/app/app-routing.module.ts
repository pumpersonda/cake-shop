import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './core/home/home.component';
import {LoginPopupComponent} from './login/login-dialog.component';
import {AuthGuardService} from './services/auth-guard.service';


const appRoutes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      {
        path: 'login',
        component: LoginPopupComponent
      }
    ]
  },
  {path: 'product', loadChildren: './product/product.module#ProductModule'},
  {path: 'client', loadChildren: './client/client.module#ClientModule', canLoad: [AuthGuardService], canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {useHash: true, preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {

}

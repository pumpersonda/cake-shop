export class Client {
  constructor(public id?: string,
              public name?: string,
              public age?: number,
              public phone?: string) {
  }

}

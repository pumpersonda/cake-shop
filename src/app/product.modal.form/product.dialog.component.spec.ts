import {async, fakeAsync, TestBed} from '@angular/core/testing';
import {ProductDialogComponent} from './product-dialog.component';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from '../services/product.service';
import {Product} from '../models/product.model';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FileUploadService} from '../services/file.upload.service';


describe('ProductDialogComponent', function () {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductDialogComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        ProductService,
        FileUploadService,
        NgbActiveModal]
    }).compileComponents();
  }));

  it('should create the ProductComponent', async(() => {
    const fixture = TestBed.createComponent(ProductDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should create a form', async(() => {
    const fixture = TestBed.createComponent(ProductDialogComponent);
    const app = fixture.debugElement.componentInstance;
    app.product = new Product();
    fixture.detectChanges();
    const element = fixture.debugElement.nativeElement;
    expect(element.querySelector('form')).toBeTruthy();
  }));

});

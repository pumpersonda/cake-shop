import {Component, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../models/product.model';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../services/product.service';
import {FileUploadService} from '../services/file.upload.service';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product.dialog.component.css']
})
export class ProductDialogComponent {
  product: Product;
  fileToUpload: File = null;
  fileBase64: Blob | string = '../../assets/birthday-cake.png';

  constructor(private productService: ProductService,
              public activeModal: NgbActiveModal,
              private fileUploadService: FileUploadService) {

  }

  submit() {
    if (this.fileToUpload) {
      this.uploadFileToActivity()
        .then((res: HttpResponse<any>) => {
          this.product.image = res.body.url;
          this.save();
        });
    } else {
      this.save();
    }
  }

  save() {
    if (this.product.id) {
      this.productService.update(this.product);
    } else {
      this.productService.create(this.product);
    }
    this.activeModal.dismiss('Saved!');
  }

  clear() {
    this.activeModal.dismiss('Cancel');
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileUploadService
      .getBase64(this.fileToUpload)
      .then((result: Blob) => {
        this.fileBase64 = result;
      });
  }

  uploadFileToActivity() {
    return this.fileUploadService.postFile(this.fileToUpload);
  }
}


@Component({
  selector: 'app-product-popup',
  template: ''
})
export class ProductPopupComponent implements OnInit, OnDestroy {
  routeSub: any;

  constructor(private route: ActivatedRoute,
              private modalService: NgbModal,
              private router: Router,
              private productService: ProductService) {
  }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe((params) => {
      if (params['id']) {
        this.productService.find(params['id'])
          .subscribe(
            (response) => {
              const product: Product = response.body as Product;
              product.id = params['id'];
              this.openModal(Object.assign({}, product) as Product);
            },
            (err) => console.log(err)
          );
      } else {
        const product = new Product();
        this.openModal(product);
      }
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  private openModal(product: Product) {
    setTimeout(() => {
      const modalRef = this.modalService.open(ProductDialogComponent as Component, {size: 'lg', backdrop: 'static'});
      modalRef.componentInstance.product = product;
      modalRef.result.then((result) => {
        this.router.navigate(['/product']);
      }, (reason) => {
        this.router.navigate(['/product']);
      });
    }, 0);
  }
}

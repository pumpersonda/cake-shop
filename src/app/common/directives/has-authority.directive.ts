import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Directive({
  selector: '[appHasAuthority]'
})
export class HasAuthorityDirective implements OnInit {

  constructor(private authService: AuthService,
              private elementRef: ElementRef,
              private renderer: Renderer2) {
  }

  public ngOnInit(): void {
    this.authService.isAuthenticated()
      .then(
        (isAuthenticated: boolean) => {
          if (!isAuthenticated) {
            this.renderer.setStyle(
              this.elementRef.nativeElement,
              'visibility',
              'hidden'
            );
          }
        }
      );
  }


}

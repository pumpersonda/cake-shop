import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HasAuthorityDirective} from './directives/has-authority.directive';


@NgModule({
  declarations: [
    HasAuthorityDirective
  ],
  exports: [
    CommonModule,
    HasAuthorityDirective
  ]
})
export class SharedModule {

}

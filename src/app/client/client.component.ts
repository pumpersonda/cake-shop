import {Component, OnInit} from '@angular/core';
import {ClientService} from '../services/client.service';
import {Client} from '../models/client.model';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Product} from '../models/product.model';


@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  clients: Client[];
  clientService: ClientService;
  constructor(private clientSer: ClientService) {
  }

  ngOnInit() {
    this.clientService = this.clientSer;
    this.loadAll();
    this.clientSer.clientListChanged
      .subscribe(() => this.loadAll());
  }

  loadAll() {
    this.clientSer.query()
      .subscribe(
        (res: HttpResponse<Client[]>) => this.clients = res.body,
        (res: HttpErrorResponse) => console.error(res)
      );
  }

}

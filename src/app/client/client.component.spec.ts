import {async, fakeAsync, getTestBed, TestBed, tick} from '@angular/core/testing';
import {ClientComponent} from './client.component';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ClientService} from '../services/client.service';
import {AuthService} from '../services/auth.service';
import {AuthGuardService} from '../services/auth-guard.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {SERVER_API_URL} from '../common/constants';
import {Client} from '../models/client.model';


describe('ClientComponent', function () {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClientComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        ClientService,
        AuthService,
        AuthGuardService,
        NgbActiveModal]
    }).compileComponents();
  }));

  it('should create the ClientDeleteDialogComponent', async(() => {
    const fixture = TestBed.createComponent(ClientComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
describe('ClientService', () => {
  let injector: TestBed;
  let service: ClientService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ClientService]
    });
    injector = getTestBed();
    service = injector.get(ClientService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('Service methods', () => {

    it('should call correct URL', () => {
      service.find('123').subscribe(() => {
      });

      const req = httpMock.expectOne({method: 'GET'});

      const resourceUrl = SERVER_API_URL + 'clients';
      expect(req.request.url).toEqual(resourceUrl + '/' + '123.json');
    });

    it('should return Client', () => {
      service.find('123').subscribe((received) => {
        const client: Client = received.body as Client;
        expect(client.id).toEqual('123');
      });

      const req = httpMock.expectOne({method: 'GET'});
      req.flush({id: '123'});
    });

    it('should return clients', () => {
      const products = [
        new Client('1', 'product1', 22),
        new Client('2', 'product2', 23)];
      service.query().subscribe((productsResponse) => {
        expect(productsResponse.body.length).toEqual(2);
        expect(productsResponse.body).toEqual(products);
      });

      const resourceUrl = SERVER_API_URL + 'clients';
      const req = httpMock.expectOne(`${resourceUrl}.json`);
      expect(req.request.method).toBe('GET');
      req.flush(products);
    });
  });
});

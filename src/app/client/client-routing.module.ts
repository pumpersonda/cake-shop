import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from '../services/auth-guard.service';
import {ClientDeletePopupComponent} from '../client.delete.dialog/client-delete-dialog.component';
import {ClientComponent} from './client.component';
import {ClientPopupComponent} from '../client.modal.form/client-dialog.component';


const clientRoute: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      {
        path: 'new',
        component: ClientPopupComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'edit/:id',
        component: ClientPopupComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'delete/:id',
        component: ClientDeletePopupComponent,
        canActivate: [AuthGuardService]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(clientRoute),
    NgbModule
  ],
  exports: [RouterModule]
})
export class ClientRoutingModule {
}

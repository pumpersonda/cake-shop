import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ClientComponent} from './client.component';
import {ClientService} from '../services/client.service';
import {ClientDialogComponent, ClientPopupComponent} from '../client.modal.form/client-dialog.component';
import {ClientDeleteDialogComponent, ClientDeletePopupComponent} from '../client.delete.dialog/client-delete-dialog.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ClientRoutingModule} from './client-routing.module';




@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ClientRoutingModule
  ],
  declarations: [
    ClientComponent,
    ClientPopupComponent,
    ClientDialogComponent,
    ClientDeletePopupComponent,
    ClientDeleteDialogComponent
  ],
  entryComponents: [
    ClientPopupComponent,
    ClientDialogComponent,
    ClientDeletePopupComponent,
    ClientDeleteDialogComponent
  ],
  providers: [
    ClientService
  ]
})
export class ClientModule {
}

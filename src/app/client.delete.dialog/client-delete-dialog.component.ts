import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ClientService} from '../services/client.service';
import {Client} from '../models/client.model';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-client-delete-dialog',
  templateUrl: './client-delete-dialog.component.html'
})
export class ClientDeleteDialogComponent {
  client: Client;

  constructor(private clientService: ClientService,
              public activeModal: NgbActiveModal) {

  }

  confirmDelete() {
    this.clientService.delete(this.client.id);
    this.activeModal.dismiss('Client deleted!');
  }

  clear() {
    this.activeModal.dismiss('Cancel');
  }

}


@Component({
  selector: 'app-client-delete-popup',
  template: ''
})
export class ClientDeletePopupComponent implements OnInit, OnDestroy {
  routeSub: any;

  constructor(private route: ActivatedRoute,
              private modalService: NgbModal,
              private router: Router,
              private clientService: ClientService) {
  }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe((params) => {
      if (params['id']) {
        this.clientService.find(params['id'])
          .subscribe(
            (response) => {
              const client: Client = response.body as Client;
              client.id = params['id'];
              this.openModal(Object.assign({}, client));
            },
            (err) => console.log(err)
          );
      }
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  private openModal(client: Client) {
    setTimeout(() => {
      const modalRef = this.modalService.open(ClientDeleteDialogComponent as Component, {size: 'lg', backdrop: 'static'});
      modalRef.componentInstance.client = client;
      modalRef.result.then((result) => {
        this.router.navigate(['client']);
      }, (reason) => {
        this.router.navigate(['client']);
      });
    }, 0);
  }
}

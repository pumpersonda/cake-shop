import {async, TestBed} from '@angular/core/testing';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {ClientService} from '../services/client.service';
import {AuthService} from '../services/auth.service';
import {AuthGuardService} from '../services/auth-guard.service';
import {Client} from '../models/client.model';
import {ClientDeleteDialogComponent} from './client-delete-dialog.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';


describe('ClientDeleteDialogComponent', function () {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClientDeleteDialogComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        ClientService,
        AuthService,
        AuthGuardService,
        NgbActiveModal]
    }).compileComponents();
  }));

  it('should create the ClientDeleteDialogComponent', async(() => {
    const fixture = TestBed.createComponent(ClientDeleteDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should create a form', async(() => {
    const fixture = TestBed.createComponent(ClientDeleteDialogComponent);
    const app = fixture.debugElement.componentInstance;
    app.client = new Client();
    fixture.detectChanges();
    const element = fixture.debugElement.nativeElement;
    expect(element.querySelector('form')).toBeTruthy();
  }));

});

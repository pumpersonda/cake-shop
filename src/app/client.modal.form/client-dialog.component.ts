import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ClientService} from '../services/client.service';
import {Client} from '../models/client.model';
import {Product} from '../models/product.model';


@Component({
  selector: 'app-client-dialog',
  templateUrl: './client-dialog.component.html'
})
export class ClientDialogComponent implements OnInit {
  client: Client;
  clientService: ClientService;

  constructor(public activeModal: NgbActiveModal,
              private service: ClientService) {
  }

  ngOnInit() {
    this.clientService = this.service;
  }

  save() {
    if (this.client.id) {
      this.service.update(this.client);
    } else {
      this.service.create(this.client);
    }
    this.activeModal.dismiss('Save');
  }

  clear() {
    this.activeModal.dismiss('Cancel');
  }
}


@Component({
  selector: 'app-client-popup',
  template: ''
})
export class ClientPopupComponent implements OnInit, OnDestroy {
  routeSub: any;

  constructor(private route: ActivatedRoute,
              private modalService: NgbModal,
              private router: Router,
              private clientService: ClientService) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      if (params['id']) {
        this.clientService.find(params['id'])
          .subscribe(
            (response) => {
              const client: Client = response.body as Client;
              client.id = params['id'];
              this.openModal(Object.assign({}, client));
            },
            (err) => console.log(err)
          );
      } else {
        const client = new Client();
        this.openModal(client);
      }

    });
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }


  private openModal(client: Client) {
    setTimeout(() => {
      const modalRef = this.modalService.open(ClientDialogComponent as Component, {size: 'lg', backdrop: 'static'});
      modalRef.componentInstance.client = client;
      modalRef.result.then((result) => {
        this.router.navigate(['client']);
      }, (reason) => {
        this.router.navigate(['client']);
      });
    }, 0);
  }
}

import {async, fakeAsync, TestBed} from '@angular/core/testing';
import {ClientDialogComponent} from './client-dialog.component';
import {FormsModule} from '@angular/forms';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Client} from '../models/client.model';
import {AuthService} from '../services/auth.service';
import {RouterTestingModule} from '@angular/router/testing';
import {ClientService} from '../services/client.service';
import {AuthGuardService} from '../services/auth-guard.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ClientDialogComponent', function () {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClientDialogComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        ClientService,
        AuthService,
        AuthGuardService,
        NgbActiveModal]
    }).compileComponents();
  }));


  it('should create the ClientDialogComponent', async(() => {
    const fixture = TestBed.createComponent(ClientDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));


  it('should create a form', async(() => {
    const fixture = TestBed.createComponent(ClientDialogComponent);
    const app = fixture.debugElement.componentInstance;
    app.client = new Client();
    fixture.detectChanges();
    const element = fixture.debugElement.nativeElement;
    expect(element.querySelector('form')).toBeTruthy();
  }));

});

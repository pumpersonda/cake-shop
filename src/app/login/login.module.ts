import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {LoginDialogComponent, LoginPopupComponent} from './login-dialog.component';
import {AuthService} from '../services/auth.service';
import {AuthGuardService} from '../services/auth-guard.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    LoginDialogComponent,
    LoginPopupComponent
  ],
  entryComponents: [
    LoginDialogComponent,
    LoginPopupComponent
  ],
  providers: [
    AuthService,
    AuthGuardService
  ]
})
export class LoginModule {
}

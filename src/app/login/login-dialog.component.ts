import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../services/auth.service';


@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent {
  user: { username: string, password: string };

  constructor(public activeModal: NgbActiveModal, private authService: AuthService) {
    this.user = {'username': '', 'password': ''};
  }

  save() {
    this.authService.login();
    this.activeModal.dismiss('Login');
  }

  clear() {
    this.activeModal.dismiss('Close');
  }

  isValid() {
    return this.user.username !== '' && this.user.password !== '';
  }

}


@Component({
  selector: 'app-login-popup',
  template: ''
})
export class LoginPopupComponent implements OnInit, OnDestroy {
  constructor(private route: ActivatedRoute,
              private modalService: NgbModal,
              private router: Router) {
  }

  ngOnInit(): void {
    this.openModal();
  }

  ngOnDestroy(): void {
  }

  private openModal() {
    setTimeout(() => {
      const modalRef = this.modalService.open(LoginDialogComponent as Component, {size: 'sm', backdrop: 'static'});
      modalRef.result.then((result) => {
        this.router.navigate(['']);
      }, (reason) => {
        this.router.navigate(['']);
      });
    }, 0);
  }

}

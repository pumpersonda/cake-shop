import {TestBed, async, fakeAsync, tick} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {LoginDialogComponent, LoginPopupComponent} from './login-dialog.component';
import {FormsModule} from '@angular/forms';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthGuardService} from '../services/auth-guard.service';
import {AuthService} from '../services/auth.service';


describe('LoginPopupComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginPopupComponent,
        LoginDialogComponent
      ],
      imports: [RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        AuthService,
        AuthGuardService,
        NgbActiveModal
      ]
    }).compileComponents();
  }));

  it('should create the LoginPopupComponent', async(() => {
    const fixture = TestBed.createComponent(LoginPopupComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should create the LoginDialogComponent', async(() => {
    const fixture = TestBed.createComponent(LoginDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should create a form', async(() => {
    const fixture = TestBed.createComponent(LoginDialogComponent);
    fixture.detectChanges();
    const element = fixture.debugElement.nativeElement;
    expect(element.querySelector('form')).toBeTruthy();
  }));

  it('should disabled the button if the inputs are empty', fakeAsync(() => {
    const fixture = TestBed.createComponent(LoginDialogComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    tick();
    fixture.detectChanges();
    expect(compiled.querySelector('button[type="submit"]').disabled).toEqual(true);
  }));

  it('should enable the button if the inputs are not empty', fakeAsync(() => {
    const fixture = TestBed.createComponent(LoginDialogComponent);
    const app = fixture.debugElement.componentInstance;
    app.user.username = 'fake@hotmail.com';
    app.user.password = '123';
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    tick();
    fixture.detectChanges();
    expect(compiled.querySelector('button[type="submit"]').disabled).toEqual(false);
  }));


  it('should disable the button if the inputs are spaces', fakeAsync(() => {
    const fixture = TestBed.createComponent(LoginDialogComponent);
    const app = fixture.debugElement.componentInstance;
    app.user.username = '\n';
    app.user.password = '\n';
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    tick();
    fixture.detectChanges();
    expect(compiled.querySelector('button[type="submit"]').disabled).toEqual(true);
  }));

});

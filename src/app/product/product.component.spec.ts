import {TestBed, async, fakeAsync, tick, getTestBed} from '@angular/core/testing';
import {ProductComponent} from './product.component';
import {ProductService} from '../services/product.service';
import {RouterTestingModule} from '@angular/router/testing';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {SERVER_API_URL} from '../common/constants';
import {Product} from '../models/product.model';

describe('ProductComponent', function () {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductComponent
      ],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule, NgbModule.forRoot()],
      providers: [
        ProductService,
        NgbActiveModal]
    }).compileComponents();
  }));

  it('should create the ProductComponent', async(() => {
    const fixture = TestBed.createComponent(ProductComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});


describe('ProductService', () => {
  let injector: TestBed;
  let service: ProductService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService]
    });
    injector = getTestBed();
    service = injector.get(ProductService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('Service methods', () => {

    it('should call correct URL', () => {
      service.find('123').subscribe(() => {
      });

      const req = httpMock.expectOne({method: 'GET'});

      const resourceUrl = SERVER_API_URL + 'products';
      expect(req.request.url).toEqual(resourceUrl + '/' + '123.json');
    });

    it('should return Product', () => {
      service.find('123').subscribe((received) => {
        expect(received.body.id).toEqual('123');
      });

      const req = httpMock.expectOne({method: 'GET'});
      req.flush({id: '123'});
    });

    it('should return products', () => {
      const products = [
        new Product('1', 'product1', 100),
        new Product('2', 'product2', 200)];
      service.query().subscribe((productsResponse) => {
        expect(productsResponse.body.length).toEqual(2);
        expect(productsResponse.body).toEqual(products);
      });

      const resourceUrl = SERVER_API_URL + 'products';
      const req = httpMock.expectOne(`${resourceUrl}.json`);
      expect(req.request.method).toBe('GET');
      req.flush(products);
    });
  });
});

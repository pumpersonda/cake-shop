import {NgModule} from '@angular/core';
import {ProductPopupComponent} from '../product.modal.form/product-dialog.component';
import {ProductComponent} from './product.component';
import {ProductDeletePopupComponent} from '../product.delete.dialog/product-delete-dialog.component';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


const productRoute: Routes = [
  {
    path: '',
    component: ProductComponent,
    children: [
      {
        path: 'new',
        component: ProductPopupComponent
      },
      {
        path: 'edit/:id',
        component: ProductPopupComponent
      },
      {
        path: 'delete/:id',
        component: ProductDeletePopupComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoute),
    NgbModule
  ],
  exports: [RouterModule]
})
export class ProductRoutingModule {}

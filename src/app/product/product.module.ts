import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProductComponent} from './product.component';
import {ProductDialogComponent, ProductPopupComponent} from '../product.modal.form/product-dialog.component';
import {FormsModule} from '@angular/forms';
import {ProductDeleteDialogComponent, ProductDeletePopupComponent} from '../product.delete.dialog/product-delete-dialog.component';
import {ProductRoutingModule} from './product-routing.module';
import {SharedModule} from '../common/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ProductRoutingModule
  ],
  declarations: [
    ProductComponent,
    ProductDialogComponent,
    ProductPopupComponent,
    ProductDeleteDialogComponent,
    ProductDeletePopupComponent
  ],
  entryComponents: [
    ProductComponent,
    ProductDialogComponent,
    ProductPopupComponent,
    ProductDeleteDialogComponent,
    ProductDeletePopupComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ProductModule {
}

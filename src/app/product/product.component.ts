import {Component, OnInit} from '@angular/core';
import {Product} from '../models/product.model';
import {ProductService} from '../services/product.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products: Product[];

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.loadAll();
    this.productService.productListChanged
      .subscribe(() => this.loadAll());
  }

  loadAll() {
    this.productService.query()
      .subscribe(
        (res: HttpResponse<Product[]>) => this.products = res.body,
        (res: HttpErrorResponse) => console.error(res)
      );
  }
}

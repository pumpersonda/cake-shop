import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';


import {AppComponent} from './app.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthService} from './services/auth.service';
import {CoreModule} from './core/core.module';
import {SharedModule} from './common/shared.module';
import {AppRoutingModule} from './app-routing.module';
import {ProductService} from './services/product.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginModule} from './login/login.module';
import {HttpClientModule} from '@angular/common/http';
import {FileUploadService} from './services/file.upload.service';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    CoreModule,
    SharedModule,
    NgbModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    ProductService,
    AuthService,
    AuthGuardService,
    FileUploadService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  authService: AuthService;
  constructor(private authS: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.authService = this.authS;
  }


  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}

import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NavbarComponent} from './navbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {SharedModule} from '../../common/shared.module';


@NgModule({
  imports: [
    NgbModule.forRoot(),
    RouterModule,
    SharedModule],

  declarations: [
    NavbarComponent
  ],
  exports: [NavbarComponent],
  providers: [AuthService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NavbarModule {
}

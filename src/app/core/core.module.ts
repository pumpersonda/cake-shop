import {NgModule} from '@angular/core';
import {HomeModule} from './home/home.module';
import {NavbarModule} from './navbar/navbar.module';


@NgModule({
  imports: [
    HomeModule,
    NavbarModule
  ],
  declarations: [],
  exports: [
    NavbarModule
  ]
})
export class CoreModule {
}

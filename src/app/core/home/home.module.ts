import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {HomeComponent} from './home.component';
import {NavbarModule} from '../navbar/navbar.module';
import {RouterModule} from '@angular/router';
import {AuthService} from '../../services/auth.service';


@NgModule({
  imports: [
    NavbarModule,
    RouterModule
  ],
  declarations: [
    HomeComponent
  ],
  providers: [AuthService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeModule {}

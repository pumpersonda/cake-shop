import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {
  loggedIn = false;

  isAuthenticated() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log(this.loggedIn);
        resolve(this.loggedIn);
      }, 0);
    });
  }

  login() {
    this.loggedIn = true;
  }

  logout() {
    this.loggedIn = false;
  }

}

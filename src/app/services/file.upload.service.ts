import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {CLOUDINARY_URL} from '../common/constants';

@Injectable()
export class FileUploadService {
  constructor(private http: HttpClient) {
  }

  postFile(fileToUpload: File) {
    const form = new FormData();
    form.append('file', fileToUpload);
    form.append('upload_preset', 'bfgniu9i');
    form.append('tags', 'browser_upload');
    return this.http.post(CLOUDINARY_URL, form, {observe: 'response'})
      .toPromise()
      .then((res: HttpResponse<Object>) => {
        return res;
      })
      .catch((err) => console.log(err));
  }

  getBase64(file): Promise<Blob> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        resolve(reader.result);
      };
      reader.onerror = (err) => {
        reject(err);
      };
    });
  }
}


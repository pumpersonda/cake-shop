import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../common/constants';
import {Product} from '../models/product.model';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {
  private resourceUrl = SERVER_API_URL + 'products';
  productListChanged = new EventEmitter<Product[]>();

  constructor(private http: HttpClient) {
  }

  create(product: Product) {
    return this.http.post(`${this.resourceUrl}.json`, product)
      .toPromise()
      .then(() => this.emit());
  }

  find(id: string) {
    return this.http.get<Product>(`${this.resourceUrl}/${id}.json`, {observe: 'response'})
      .map((res: HttpResponse<Product>) => Object.assign({}, res));
  }

  query(): Observable<HttpResponse<Product[]>> {
    return this.http.get<Product[]>(`${this.resourceUrl}.json`, {observe: 'response'})
      .map((res: HttpResponse<Product[]>) => this.convertArrayResponse(res));
  }

  update(product: Product) {
    return this.http.put(`${this.resourceUrl}/${product.id}.json`, product)
      .toPromise()
      .then(() => this.emit());
  }

  delete(id: string) {
    return this.http.delete(`${this.resourceUrl}/${id}.json`)
      .toPromise()
      .then(() => this.emit());
  }

  private emit() {
    this.productListChanged.emit();
  }

  private convertArrayResponse(res: HttpResponse<Product[]>): HttpResponse<Product[]> {
    const jsonResponse: Product[] = res.body;
    const body: Product[] = [];
    for (const productId in jsonResponse) {
      if (jsonResponse[productId]) {
        jsonResponse[productId].id = productId;
        body.push(jsonResponse[productId]);
      }

    }
    return res.clone({body});
  }

}

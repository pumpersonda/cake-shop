import {EventEmitter, Injectable} from '@angular/core';
import {Client} from '../models/client.model';
import {SERVER_API_URL} from '../common/constants';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class ClientService {
  private resourceUrl = SERVER_API_URL + 'clients';
  clientListChanged = new EventEmitter<Client[]>();

  constructor(private http: HttpClient) {
  }

  create(client: Client) {
    return this.http.post(`${this.resourceUrl}.json`, client)
      .toPromise()
      .then(() => this.emit());
  }

  find(id: string) {
    return this.http.get<Client[]>(`${this.resourceUrl}/${id}.json`, {observe: 'response'})
      .map((res: HttpResponse<Client[]>) => Object.assign({}, res));
  }

  query(): Observable<HttpResponse<Client[]>> {
    return this.http.get<Client[]>(`${this.resourceUrl}.json`, {observe: 'response'})
      .map((res: HttpResponse<Client[]>) => this.convertArrayResponse(res));
  }

  update(product: Client) {
    return this.http.put(`${this.resourceUrl}/${product.id}.json`, product)
      .toPromise()
      .then(() => this.emit());
  }

  delete(id: string) {
    return this.http.delete(`${this.resourceUrl}/${id}.json`)
      .toPromise()
      .then(() => this.emit());
  }

  private emit() {
    this.clientListChanged.emit();
  }

  private convertArrayResponse(res: HttpResponse<Client[]>): HttpResponse<Client[]> {
    const jsonResponse: Client[] = res.body;
    const body: Client[] = [];
    for (const clientId in jsonResponse) {
      if (jsonResponse[clientId]) {
        jsonResponse[clientId].id = clientId;
        body.push(jsonResponse[clientId]);
      }

    }
    return res.clone({body});
  }
}
